from turtle import done
from SobokanPlay import *
from SobokanStage import *
#Gra Sobokan
#@author: Wojciech Wolny
#główna funkcja startująca grę
#nazwa pliku jest wczytywana z konsoli
#Program został podzielony na trzy części:
#1)widok za, który odpowida plik SobokanStage
#2)zmiana modelu i reakcje na zachowanie użytkownika za co odpowiada plik SobokanPlay
#3)logikę, która sprawdza, czy magazynier może poruszyć się w wskazanym kierunku na podstawie struktury danych ...
#... zawierajacej informacje o wszystkich polach
#Gra toczy się do ustawienia pierwszego pudełka na czerwonym polu

def main():
    #filename = input("Podaj nazwe pliku: ")
    #JESLI chcemy wczytac plik z kodu to należy skomentować linijkę powyżej i odkomentowac linjke pod spodem
    filename = "Stage"
    currentMap = createStage(filename)
    ini_grafika()
    position = drawStage(currentMap)
    drawGame(currentMap, position)
    ini_klawiatura()
    play(currentMap, position)
    done()

main()
