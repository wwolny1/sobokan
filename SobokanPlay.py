from turtle import update, onkey, listen, window_width, window_height
from time import sleep
import tkinter as TK
import linecache
from Position import *
from SobokanStage import *
from Stockman import *

#Plik zarzadzajacy kontaktem z uzytkownikiem

# ------------- Obsługa klawiatury - początek-------------

zdarzenie_klawiatury = ""

def ustaw_kierunek(zd):
    def result():
        global zdarzenie_klawiatury
        zdarzenie_klawiatury = zd
        print("ustawiam zdarzenie: " + zd)
    return result

def pass_event():
    global zdarzenie_klawiatury
    while zdarzenie_klawiatury == "":
        TK._default_root.update()
        sleep(0.01)
    pom = zdarzenie_klawiatury
    zdarzenie_klawiatury = ""
    return pom

def ini_klawiatura():
    for kierunek in ["Up", "Left", "Right", "Down", "k"]:
        onkey(ustaw_kierunek(kierunek.lower()), kierunek)

    listen()

# ------------- Obsługa klawiatury - koniec-------------

def play(map, position):
    trip = True
    update()
    for row in map:
        if row.count(['.', 'M']) > 0:
            rowId=map.index(row)
            break
    columnId = map[rowId].index(['.', 'M'])
    movNr =0
    width = window_width()
    height = window_height()
    drawTitle(width, height, movNr, columnId, rowId)
    # Zwraca 0 jesli nie mozna sie ruszyc
    # zwraca 1 jesli mozna ruszyc sie i nic sie nie dzieje
    # zwraca 2 jesli ruszamy sie i przesuwamy skrzynie
    # zwraca 3 jesli ruszamy sie, przesuwamy skrzynie i skrzynia wpada do dziury
    # zwraca 4 jesli ruszamy sie i wpadamy do dziury
    # zwraca 5 jesli przesuwamy skrzynie na cel

    while trip:
        event = pass_event()
        if event == "left":    # lewo
            tmp = canGoLeft(columnId, rowId, map)
            if tmp == 1:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                map[rowId][columnId][1]=' '
                columnId-=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 2:
                drawEmpty(columnId, rowId, position)
                drawEmpty(columnId-1, rowId, position)
                map[rowId][columnId][1]=' '
                columnId-=1
                drawMen(columnId, rowId, position)
                drawBox(columnId-1, rowId, position)
                map[rowId][columnId][1]='M'
                map[rowId][columnId-1][1]='*'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 3:
                drawEmpty(columnId, rowId, position)
                drawEmpty(columnId-1, rowId, position)
                map[rowId][columnId][1]=' '
                columnId-=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 4:
                drawEmpty(columnId, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("GAME OVER!")
            if tmp == 5:
                drawEmpty(columnId, rowId, position)
                drawEmpty(columnId-1, rowId, position)
                columnId-=1
                drawMen(columnId, rowId, position)
                drawBox(columnId-1, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("Winner!")
            update()
        elif event == "up":  # góra
            tmp = canGoUp(columnId, rowId, map)
            if tmp == 1:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                map[rowId][columnId][1]=' '
                rowId-=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 2:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId-1, position)
                map[rowId][columnId][1]=' '
                rowId-=1
                drawMen(columnId, rowId, position)
                drawBox(columnId, rowId-1, position)
                map[rowId][columnId][1]='M'
                map[rowId-1][columnId][1]='*'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 3:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId+1, position)
                map[rowId][columnId][1]=' '
                rowId-=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 4:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("GAME OVER!")
            if tmp == 5:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId-1, position)
                rowId+=1
                drawMen(columnId, rowId, position)
                drawBox(columnId, rowId-1, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("Winner!")
            update()
        elif event == "right":  # prawo
            tmp = canGoRight(columnId, rowId, map)
            if tmp == 1:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                map[rowId][columnId][1]=' '
                columnId+=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 2:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId+1, rowId, position)
                map[rowId][columnId][1]=' '
                columnId+=1
                drawMen(columnId, rowId, position)
                drawBox(columnId+1, rowId, position)
                map[rowId][columnId][1]='M'
                map[rowId][columnId+1][1]='*'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 3:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId+1, rowId, position)
                map[rowId][columnId][1]=' '
                columnId+=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 4:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("GAME OVER!")
            if tmp == 5:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId+1, rowId, position)
                columnId+=1
                drawMen(columnId, rowId, position)
                drawBox(columnId+1, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("Winner!")
            update()
        elif event == "down":  # dół
            tmp = canGoDown(columnId, rowId, map)
            if tmp == 1:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                map[rowId][columnId][1]=' '
                rowId+=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 2:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId+1, position)
                map[rowId][columnId][1]=' '
                rowId+=1
                drawMen(columnId, rowId, position)
                drawBox(columnId, rowId+1, position)
                map[rowId][columnId][1]='M'
                map[rowId+1][columnId][1]='*'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 3:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId+1, position)
                map[rowId][columnId][1]=' '
                rowId+=1
                drawMen(columnId, rowId, position)
                map[rowId][columnId][1]='M'
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
            if tmp == 4:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("GAME OVER!")
            if tmp == 5:
                drawField(map[rowId][columnId][0], columnId, rowId, position)
                drawEmpty(columnId, rowId+1, position)
                rowId+=1
                drawMen(columnId, rowId, position)
                drawBox(columnId, rowId+1, position)
                movNr+=1
                drawTitle(width, height, movNr, columnId, rowId)
                trip =False
                endGameSign("Winner!")
            update()
        elif event == "k":   # koniec
            trip = False
            endGameSign("GAME OVER!")
        else:
            print("Nieobsługiwane zdarzenie: " + event)
