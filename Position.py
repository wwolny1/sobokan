#klasa pomocnicza
#Zwraca pozyjce wg indeksow 'i' i 'j' na planszy o podanych rozmiarach
#getX i getY zwraca lewy dolny rog pola
#Postion.getX(i,j) = width/2 - (nrWidth-i)*(Width/nrWidth)
#Position.getY(i,j) = (nrHeight-j-1)*(Height/nrHeight) -height/2
class Position:
    width = 0   #szerokosz okna
    height = 0  #wysokosc okna
    nrWidth = 0 #ile okien na szerokosc
    nrHeight = 0#ile okien na wysokosc
    fieldHeight = 0
    fieldWidth = 0

    def __init__(self, width, heigth, nrWidth, nrHeight):
        self.width = width
        self.height = heigth
        self.nrWidth = nrWidth
        self.nrHeight = nrHeight
        self.fieldHeight = heigth/nrHeight
        self.fieldWidth = width/nrWidth

    def getX(self, columnId):
        return self.width/2 - (self.nrWidth-columnId)*(self.width/self.nrWidth)
    def getY(self, rowId):
        return (self.nrHeight-rowId-1)*(self.height/self.nrHeight) - self.height/2
