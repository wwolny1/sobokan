#Plik zarzadzajacy logika gry
#zezwala magazynierowi na ruch w podana strone
#do kazdej funkcji jest podawane miejsce w ktorym stoi magazynier

# Zwraca 0 jesli nie mozna sie ruszyc
# zwraca 1 jesli mozna ruszyc sie i nic sie nie dzieje
# zwraca 2 jesli ruszamy sie i przesuwamy skrzynie
# zwraca 3 jesli ruszamy sie, przesuwamy skrzynie i skrzynia wpada do dziury
# zwraca 4 jesli ruszamy sie i wpadamy do dziury
# zwraca 5 jesli przesuwamy skrzynie na cel

def canGoRight(columnId, rowId, map):
    if len(map[0]) <= columnId+1:
        return 0
    pair = map[rowId][columnId+1]
    if pair[0] == '#':
        return 0
    if pair[1] == ' ':
        if pair[0] == 'D':
            return 4
        else:
            return 1
    if len(map[0]) <= columnId+2:
        return 0
    if pair[1] == '*':
        tmp = map[rowId][columnId+2]
        if tmp[1] == '*' or tmp[0] == '#':
            return 0
        if tmp[0] == 'D':
            return 3
        if tmp[0] == '!':
            return 5
        return 2
    return 0

def canGoLeft(columnId, rowId, map):
    if columnId - 1 < 0:
        return 0
    pair = map[rowId][columnId-1]
    if pair[0] == '#':
        return 0
    if pair[1] == ' ':
        if pair[0] == 'D':
            return 4
        else:
            return 1
    if columnId-2 < 0:
        return 0
    if pair[1] == '*':
        tmp = map[rowId][columnId-2]
        if tmp[1] == '*' or tmp[0] == '#':
            return 0
        if tmp[0] == 'D':
            return 3
        if tmp[0] == '!':
            return 5
        return 2
    return 0

def canGoUp(columnId, rowId, map):
    if rowId-1 < 0:
        return 0
    pair = map[rowId-1][columnId]
    if pair[0] == '#':
        return 0
    if pair[1] == ' ':
        if pair[0] == 'D':
            return 4
        else:
            return 1
    if rowId-2 < 0:
        return 0
    if pair[1] == '*':
        tmp = map[rowId-2][columnId]
        if tmp[1] == '*' or tmp[0] == '#':
            return 0
        if tmp[0] == 'D':
            return 3
        if tmp[0] == '!':
            return 5
        return 2
    return 0

def canGoDown(columnId, rowId, map):
    if len(map) <= rowId+1:
        return 0
    pair = map[rowId+1][columnId]
    if pair[0] == '#':
        return 0
    if pair[1] == ' ':
        if pair[0] == 'D':
            return 4
        else:
            return 1
    if len(map) <= rowId+2:
        return 0
    if pair[1] == '*':
        tmp = map[rowId+2][columnId]
        if tmp[1] == '*' or tmp[0] == '#':
            return 0
        if tmp[0] == 'D':
            return 3
        if tmp[0] == '!':
            return 5
        return 2
    return 0
