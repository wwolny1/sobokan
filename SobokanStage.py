from turtle import mode, tracer, fd, setpos, fillcolor, color, down, up, right, window_width, window_height, pencolor
from turtle import speed, begin_fill, end_fill, circle, left, write
from Position import *

#Plik zarzadzajacy grafika programu

def ini_grafika():
    mode("logo")
    speed(0)

def drawRectangle(x, y):
    for i in [0,1]:
        fd(y)
        right(90)
        fd(x)
        right(90)

#Zeby zmienic ilosc miejsca przeznaczona na tytul i stopke wystarczy ...
#... zmienic wartosc zmiennej titleBlockHeight
def drawTitle(width, height, movNr, posX, posY):
    titleBlockHeight = 50
    up()
    setpos(-width/2, height/2 - titleBlockHeight)
    fillcolor("green")
    down()
    begin_fill()
    drawRectangle(width, titleBlockHeight)
    end_fill()
    up()
    right(90)
    fd(width/2)
    left(90)
    color("black")
    write("Gra Sobokan", align="center", font=("Arial", 30, "normal"))
    drawSubTitle(width, height, titleBlockHeight, movNr, posX, posY)
    return titleBlockHeight

def drawSubTitle(width, height, titleBlockHeight, movNr, posX, posY):
    up()
    setpos(-width/2, - height/2)
    fillcolor("green")
    down()
    begin_fill()
    drawRectangle(width, titleBlockHeight)
    end_fill()
    up()
    right(90)
    fd(width/2)
    left(90)
    fd(titleBlockHeight/4)
    color("black")
    write("liczba ruchow: "+str(movNr)+", poz=("+str(posX)+","+str(posY)+")", align="center", font=("Arial", 25, "normal"))

def createStage(filename):
    with open(filename, 'r') as file:
        sobokanMap = list()
        for row in file:
            newRow = list()
            newRow.clear()
            for element in row:
                if element == '\n':
                    continue
                if element == '.':
                    newRow.append(['.', ' '])
                elif element == '#':
                    newRow.append(['#', ' '])
                elif element == 'M':
                    newRow.append(['.', 'M'])
                elif element == 'D':
                    newRow.append(['D', ' '])
                elif element == '!':
                    newRow.append(['!', ' '])
                elif element == '*':
                    newRow.append(['.', '*'])
            tmpRow = newRow[:]
            sobokanMap.append(tmpRow)
        return sobokanMap


def drawField(field, columnId, rowId, position):
    if field == '!':
        drawDestiny(columnId, rowId, position)
    elif  field == '#':
        drawWall(columnId, rowId, position)
    elif field == 'D':
        drawHole(columnId, rowId, position)
    elif field == '.':
        drawEmpty(columnId, rowId, position)

def drawFieldBox(position):
    begin_fill()
    drawRectangle(position.fieldWidth, position.fieldHeight)
    end_fill()

def drawHole(columnId, rowId, position):
    up()
    setpos(position.getX(columnId), position.getY(rowId))
    down()
    pencolor("white")
    fillcolor("black")
    drawFieldBox(position)

def drawEmpty(columnId, rowId, position):
    up()
    setpos(position.getX(columnId), position.getY(rowId))
    down()
    pencolor("white")
    fillcolor("blue")
    drawFieldBox(position)

def drawDestiny(columnId, rowId, position):
    up()
    setpos(position.getX(columnId), position.getY(rowId))
    down()
    pencolor("white")
    fillcolor("red")
    drawFieldBox(position)

def drawWall(columnId, rowId, position):
    up()
    setpos(position.getX(columnId), position.getY(rowId))
    down()
    pencolor("white")
    fillcolor("yellow")
    drawFieldBox(position)

def drawMen(columnId, rowId, position):
    up()
    setpos(position.getX(columnId)+position.fieldWidth/2, position.getY(rowId)+position.fieldHeight/2)
    right(90)
    fd(20)
    left(90)
    down()
    pencolor("orange")
    fillcolor("orange")
    begin_fill()
    circle(20)
    end_fill()
    left(90)
    fd(20)
    right(90)

#Zeby zmienic dlugosc boku pudelka...
#... wystarczy zmienic wielkosc boxSize
def drawBox(columnId, rowId, position):
    boxSize = 30
    up()
    setpos(position.getX(columnId)+position.fieldWidth/2, position.getY(rowId)+position.fieldHeight/2)
    fd(10)
    right(90)
    fd(10)
    right(90)
    down()
    pencolor("brown")
    fillcolor("brown")
    begin_fill()
    drawRectangle(boxSize, boxSize)
    end_fill()
    fd(10)
    right(90)
    fd(10)
    right(90)

def drawStage(map):
    width = window_width()
    height = window_height()
    height = height - 2*drawTitle(width, height, 0, 0, 0)
    position = Position(width, height, len(map[0]), len(map))
    rowId=0
    while rowId < len(map):
        columnId = 0
        while columnId < len(map[0]):
            field = map[rowId][columnId][0]
            drawField(field, columnId, rowId, position)
            columnId+=1
        rowId+=1
    return position

def drawGame(map, position):
    rowId = 0
    while rowId < len(map):
        columnId = 0
        while columnId < len(map[0]):
            object = map[rowId][columnId][1]
            if object == 'M':
                drawMen(columnId, rowId, position)
            elif object == '*':
                drawBox(columnId, rowId, position)
            columnId+=1
        rowId+=1

def endGameSign(message):
    up()
    setpos(0,0)
    write(message, align="center", font=("Arial", 40, "bold"))
